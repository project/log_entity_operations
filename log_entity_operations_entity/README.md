# INTRODUCTION
When using log_entity_operations, all the entity operations get logged to the
default Drupal logging mechanism (and usually shown by dblog). However when
trying to find evidence of a certain operation, maybe a node that was deleted,
a search through the default logging channel is quite difficult.

This is where log_entity_operations_entity comes into place. When enabled, the
logs get stored as EntityOperationLog entities, which can be filtered through a
separate view which comes shipped with the module.

Which events get logged with what information is still configured in the UI
which is provided by log_entity_operations.

* Project page: https://drupal.org/project/log_entity_operations

* To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/log_entity_operations

# REQUIREMENTS
Requires log_entity_operations and views modules to be installed.

# INSTALLATION
Install as any other contrib module, no specific configuration required for
installation.

# UNINSTALLING
When trying to uninstall, the created entities have to be deleted first.
Visit /admin/modules/uninstall/entity/entity_operation_log to do so.

# CONFIGURATION
This module defines the following permissions:
     
  * **Access Entity Logs**: Allow the user to access the entity logs under
    "Administration > Configuration > Development > Log Entity Operations >
    Entity Logs"

  * **Delete all Entity Logs**: Allow the user to delete one specific log,
     or all entity logs at once.

  * **View Entity Log Diff Field**: Allow the user to view the diff field of
    the entity log.