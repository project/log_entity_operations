<?php

/**
 * @file
 * Contains
 *   \Drupal\log_entity_operations_entity\Plugin\Field\FieldFormatter\LogEntityOperationsJSONCollapsibleFormatter.
 */
namespace Drupal\log_entity_operations_entity\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Component\Utility\Html;
use Drupal\log_entity_operations\Form\LogEntityOperationsSettingsForm;

/**
 * Plugin implementation of the
 * 'log_entity_operations_json_collapsible_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "log_entity_operations_json_collapsible_formatter",
 *   label = @Translation("JSON Collapsible Formatter"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class LogEntityOperationsJSONCollapsibleFormatter extends FormatterBase {

  const STATE_OPERATION_LAYER = 1;

  const STATE_PROPERTY_LAYER = 2;

  const STATE_PLAIN_RECURSIVE_LAYER = 3;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    if ($this->isPrettyCollapsibleFormattingEnabled()) {
      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\StringLongItem $item */
      foreach ($items as $item) {
        $jsonValue = $item->getValue()['value'];
        $jsonArray = json_decode($jsonValue, TRUE);
        $data = $this->prepareRecursiveData($jsonArray);
        $elements[] = [
          '#theme' => 'log_entity_operations_json_collapsible',
          '#data' => $data,
        ];
      }
    }
    else {
      // Fallback to plain text JSON Formatting.
      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\StringLongItem $item */
      foreach ($items as $item) {
        $elements[] = [
          '#type' => 'markup',
          '#markup' => Html::escape($item->getValue()['value']),
        ];
      }
    }
    return $elements;
  }

  /**
   * Check wether pretty printed collapsible JSON formatting is enabled.
   *
   * @return bool
   */
  private function isPrettyCollapsibleFormattingEnabled() {
    return (bool) \Drupal::config(LogEntityOperationsSettingsForm::CONFIG_NAME)
      ->get('enable_collapsible_json');
  }

  /**
   * Decide wether there is recursive content or if it's empty.
   *
   * @param array $jsonData
   *
   * @return array|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  private function prepareRecursiveData(array $jsonData) {
    if (is_countable($jsonData) && count($jsonData)) {
      return $this->iterateLevel($jsonData, self::STATE_OPERATION_LAYER);
    }
    else {
      return $this->t('Empty data set');
    }
  }

  /**
   * Go recursively through all levels and prepare them for rendering.
   *
   * @param array $jsonData
   *  The traversable data.
   * @param int $layerState
   *  The current type of this level.
   *
   * @return array
   *  Prepared data for rendering.
   */
  private function iterateLevel(array $jsonData, int $layerState): array {
    $data = [];
    switch ($layerState) {
      case self::STATE_OPERATION_LAYER:
        foreach ($jsonData as $operation_label => $content) {
          if (!is_array($content)) {
            \Drupal::logger('log_entity_operations_entity')
              ->alert($this->t('Recieved unprintable non-array in operation layer: :content', [':content' => $content]));
            continue;
          }
          $data[] = [
            'type' => 'operation_layer',
            'operation' => $operation_label,
            'content' => $this->iterateLevel($content, (in_array($operation_label,['added','removed']) ? self::STATE_PLAIN_RECURSIVE_LAYER : self::STATE_PROPERTY_LAYER),),
          ];
        }
        break;
      case self::STATE_PLAIN_RECURSIVE_LAYER:
      default:
        foreach ($jsonData as $property_key => $content) {
          if (is_array($content)) {
            // Firstly check if this is a changed layer with old and new, so we
            // don't iterate in them, but display them on one row.
            if (array_key_exists('old', $content) && array_key_exists('new', $content) && count($content) == '2') {
              $sub_content[] = [
                'type' => 'changed_table_layer',
                'old' => $content['old'],
                'new' => $content['new'],
              ];
            }
            else {
              // Check if the subset are non-array values, so we want to display
              // them together in a table row.
              $first_content = reset($content);
              if (!is_array($first_content)) {
                // This is not an array, so we assume these are all string values.
                $sub_content[] = [
                  'type' => 'value_layer',
                  'values' => $content,
                ];
              }
              else {
                $sub_content = $this->iterateLevel($content, $layerState == self::STATE_PLAIN_RECURSIVE_LAYER ? self::STATE_PLAIN_RECURSIVE_LAYER : self::STATE_OPERATION_LAYER);
              }
            }
            $data[] = [
              'type' => ($layerState == self::STATE_PLAIN_RECURSIVE_LAYER ? 'property_layer_plain' : 'property_layer'),
              'property_key' => $property_key,
              'property_value' => $sub_content,
            ];
          }
          else {
            \Drupal::logger('log_entity_operations_entity')->alert(
              t('Received a non-array content with layerState :state. Data object: :content',
                [':state' => $layerState, ':content' => $content])
            );
          }
        }
        break;
    }
    return $data;
  }
}
