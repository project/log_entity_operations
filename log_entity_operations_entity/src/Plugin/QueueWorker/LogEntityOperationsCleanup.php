<?php

namespace Drupal\log_entity_operations_entity\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\log_entity_operations_entity\Entity\EntityOperationLog;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Cleanup entity operation logs.
 *
 * It is done in a queue as the operation
 * may become heavy otherwise in cron implementations.
 *
 * @QueueWorker(
 *   id = "log_entity_operations_entity_cleanup",
 *   title = @Translation("Logs cleanup"),
 *   cron = {"time" = 60}
 * )
 */
class LogEntityOperationsCleanup extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Loads and delete the logs.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new LogEntityOperationsCleanup object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (isset($data['log_id'])) {
      $log = $this->entityTypeManager->getStorage('entity_operation_log')->load($data['log_id']);
      if ($log instanceof EntityOperationLog) {
        $log->delete();
      }
    }
  }

}
