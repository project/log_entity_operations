<?php

namespace Drupal\log_entity_operations_entity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Configure Log Entity Operations to custom Entities settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'log_entity_operations_entity_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['log_entity_operations_entity.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('log_entity_operations_entity.settings');
    $form['cleanup'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cleanup settings'),
    ];

    $form['cleanup']['row_limit'] = [
      '#type' => 'select',
      '#title' => $this->t('Maximum number of rows'),
      '#options' => [
        -1 => $this->t('None'),
        100 => '100',
        1000 => '1000',
        10000 => '10000',
        100000 => '100000',
        1000000 => '1000000',
      ],
      '#default_value' => $config->get('row_limit'),
      '#description' => $this->t('If configured, the oldest entity logs will be removed periodically when exceeding this limit.'),
    ];

    $form['cleanup']['date_limit'] = [
      '#type' => 'select',
      '#title' => $this->t('Expiration date'),
      '#options' => [
        '' => $this->t('None'),
        '-1 week' => $this->t('After 1 week'),
        '-2 weeks' => $this->t('After 2 weeks'),
        '-1 month' => $this->t('After 1 month'),
        '-3 month' => $this->t('After 3 months'),
        '-6 months' => $this->t('After 6 months'),
        '-1 year' => $this->t('After 1 year'),
      ],
      '#default_value' => $config->get('date_limit'),
      '#description' => $this->t('If configured, logs older than this expiration date will be removed periodically.'),
    ];

    $form['cleanup']['cron_hint_title'] = [
      '#type' => 'markup',
      '#markup' => new TranslatableMarkup('<h3>Be sure to configure cronjobs!</h3>'),
    ];

    $form['cleanup']['cron_hint_text'] = [
      '#type' => 'markup',
      '#markup' => new TranslatableMarkup(
        'To ensure automatic deletion, be sure your drush cronjobs are running at least once every five minutes:<br>
        <ul><li><pre>drush cron</pre></li></ul>
        <br>
        <strong>On larger sites, you may want to specifically run a separate cronjob for this task, as it may take some time to finish.</strong>
        <br>
        <ul><li><pre>drush queue-run log_entity_operations_entity_cleanup</pre></li></ul>'
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('log_entity_operations_entity.settings')
      ->set('row_limit', $form_state->getValue('row_limit'))
      ->set('date_limit', $form_state->getValue('date_limit'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
