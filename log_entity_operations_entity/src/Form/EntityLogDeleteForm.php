<?php

namespace Drupal\log_entity_operations_entity\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\log_entity_operations_entity\Entity\EntityOperationLog;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a form for deleting a entity_operation_log entity.
 *
 * @ingroup log_entity_operations_entity
 */
class EntityLogDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->t('Do you really want to delete this log permanently?');
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return new Url('log_entity_operations_entity.entity_logs');
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $id = $this->getIdFromRouteMatch();
    $messenger = \Drupal::messenger();
    try {
      $log = $this->getLogEntity($id);
      if (!$log instanceof EntityOperationLog) {
        $messenger->addError($this->t('The selected log was not found.'));
        $this->redirectToLogsOverview();
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $messenger->addError($this->t('The log could not be deleted. :message', [':message' => $e->getMessage()]));
      $this->redirectToLogsOverview();
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit the form and delete the specified log.
   *
   * @param array $form
   *   The Form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Form State.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    try {
      $entity = $this->getLogEntity($this->getIdFromRouteMatch());
      if ($entity instanceof EntityOperationLog) {
        $entity->delete();
        $messenger->addStatus($this->t('The Log has been deleted successfully.'));
      }
      else {
        $messenger->addError($this->t('The selected log was not found.'));
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
      $messenger->addError($this->t('The log could not be deleted. :message', [':message' => $e->getMessage()]));
    }
    $this->redirectToLogsOverview();
  }

  /**
   * Load the log entity by ID.
   *
   * @param int $id
   *   The ID of the log entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The loaded Entity Log Entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getLogEntity($id) {
    return \Drupal::entityTypeManager()
      ->getStorage('entity_operation_log')
      ->load($id);
  }

  /**
   * Get the ID from the current URL.
   *
   * @return int|null
   *   The Log ID that was specified in the URL.
   */
  private function getIdFromRouteMatch() {
    return \Drupal::routeMatch()->getParameter('id');
  }

  /**
   * Redirect to the Logs Overview.
   */
  private function redirectToLogsOverview() {
    $response = new RedirectResponse((new Url('log_entity_operations_entity.entity_logs'))->toString());
    $response->send();
  }

}
