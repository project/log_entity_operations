<?php

namespace Drupal\log_entity_operations_entity\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\system\Form\PrepareModulesEntityUninstallForm;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class PrepareLogEntityUninstallForm.
 *
 * @package Drupal\log_entity_operations_entity\Form
 */
class PrepareLogEntityUninstallForm extends PrepareModulesEntityUninstallForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    return parent::buildForm($form, $form_state, 'entity_operation_log');
  }

  /**
   * Submit Form Target Handle.
   *
   * Has to be copy-pasted from the parent so we ensure our overridden
   * moduleBatchFinished is being called once the batch has completed.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_type_id = $form_state->getValue('entity_type_id');

    $entity_type_plural = $this->entityTypeManager->getDefinition($entity_type_id)->getPluralLabel();
    $batch = [
      'title' => t('Deleting @entity_type_plural', [
        '@entity_type_plural' => $entity_type_plural,
      ]),
      'operations' => [
        [
          [__CLASS__, 'deleteContentEntities'], [$entity_type_id],
        ],
      ],
      'finished' => [__CLASS__, 'moduleBatchFinished'],
      'progress_message' => '',
    ];
    batch_set($batch);
  }

  /**
   * Handle when the Batch process has finished.
   *
   * {@inheritDoc}
   */
  public static function moduleBatchFinished($success, $results, $operations) {
    $entity_type_plural = \Drupal::entityTypeManager()->getDefinition($results['entity_type_id'])->getPluralLabel();
    \Drupal::messenger()->addStatus(t('All @entity_type_plural have been deleted.', ['@entity_type_plural' => $entity_type_plural]));

    return new RedirectResponse(Url::fromRoute('log_entity_operations_entity.entity_logs')->setAbsolute()->toString());
  }

}
