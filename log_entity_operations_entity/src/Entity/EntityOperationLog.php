<?php

namespace Drupal\log_entity_operations_entity\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Class EntityOperationLog.
 *
 * @package Drupal\log_entity_operations_entity\Entity
 *
 * @ContentEntityType(
 *   id = "entity_operation_log",
 *   label = @Translation("Entity Operation Log"),
 *   base_table = "entity_operation_logs",
 *   fieldable = FALSE,
 *   persistent_cache = FALSE,
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\log_entity_operations_entity\Entity\EntityOperationLogViewsData",
 *     "form" = {
 *       "delete" = "Drupal\log_entity_operations_entity\Form\EntityLogDeleteForm",
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "delete-form" = "/admin/config/development/log-entity-operations/delete/{id}",
 *   }
 * )
 */
class EntityOperationLog extends ContentEntityBase implements ContentEntityInterface {

  /**
   * Get Base Field Definition of EntityOperationLog Entity.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The Entity Type.
   *
   * @return array
   *   The Base Field Definition array.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Entity Operation Log entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Entity Operation Log entity.'))
      ->setReadOnly(TRUE);

    $fields['operation_time'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Operation Time'))
      ->setDescription(t('Contains the exact date and time when this operation was performed.'))
      ->setRequired(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The user ID of the registered user, if this is not a companion'))
      ->setSettings([
        'target_type' => 'user',
        'default_value' => 0,
      ])
      ->setRequired(TRUE);

    $fields['username'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Username'))
      ->setDescription(t('The user name which is associated by the uid field, in case the user is no more present'))
      ->setDefaultValue('')
      ->setRequired(TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('The ID of the entity where the operation is performed'))
      ->setRequired(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language of the entity'))
      ->setRequired(TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type'))
      ->setDescription(t('The type of the changed entity'))
      ->setDefaultValue('')
      ->setRequired(TRUE);

    $fields['bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Bundle'))
      ->setDescription(t('The bundle of the changed entity'))
      ->setDefaultValue('')
      ->setRequired(TRUE);

    $fields['operation'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Operation'))
      ->setDescription(t('The operation that has been performed on this entity'))
      ->setDefaultValue('')
      ->setRequired(TRUE);

    $fields['diff'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Diff'))
      ->setDescription(t('If enabled, the diff of the updated entity will be stored here'))
      ->setDefaultValue('')
      ->setRequired(FALSE)
      ->setSettings([
        // This is a json string and can reasonably become large.
        'max_length' => 9999999,
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    // Override the parent postDelete to avoid invalidate the cache tags.
  }

}
