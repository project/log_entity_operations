<?php

namespace Drupal\log_entity_operations_entity\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Entity Operation Log entities.
 */
class EntityOperationLogViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here. We only need this ViewsData object so our Entity is visible and
    // usable by views, so we don't need any mutations here.
    return $data;
  }

}
