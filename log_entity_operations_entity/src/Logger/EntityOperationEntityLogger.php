<?php

namespace Drupal\log_entity_operations_entity\Logger;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\log_entity_operations\Logger\EntityOperationsLoggerInterface;

/**
 * Class EntityOperationEntityLogger.
 *
 * @package Drupal\log_entity_operations_entity\Logger
 */
class EntityOperationEntityLogger implements EntityOperationsLoggerInterface {
  use StringTranslationTrait;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * LogEntityOperationsManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Log the Update to an entity.
   *
   * {@inheritDoc}
   */
  public function logUpdate(string $operation, EntityInterface $entity, AccountProxyInterface $user, string $diff = '') {
    $now = new DrupalDateTime('now');
    $now->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));

    $this->entityTypeManager->getStorage('entity_operation_log')->create(
      [
        'operation_time' => $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        'uid' => $user->id(),
        'username' => $user->getDisplayName() ?: $this->t('Anonymous'),
        'entity_id' => $entity->id(),
        'langcode' => $entity->language()->getId(),
        'entity_type' => $entity->getEntityTypeId(),
        'bundle' => $entity->bundle(),
        'operation' => $operation,
        'diff' => $diff,
      ]
    )->save();
  }

}
