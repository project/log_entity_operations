(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.logEntityOperationsEntityView = {
    attach: function (context, settings) {
      $(once('enableCollapsible', '.view-entity-operation-logs .views-view-table tbody .views-field-diff'))
        .each(function() {
          let $diffField = $(this);

          // Attach individual expand / collapse logic to list elements.
          $(once('attachHandling', $diffField.find('.leo-expand-handle')))
            .on('click', function(e) {
              e.preventDefault();
              $(this).parent('li').toggleClass('open');
            });

          // Attach "Expand all" handle logic.
          $diffField.find('.leo-controls .leo-expand-all').on('click',function(e) {
            e.preventDefault();
            $diffField.find('li').addClass('open');
          });

          // Attach "Collapse all" handle logic.
          $diffField.find('.leo-controls .leo-collapse-all').on('click',function(e) {
            e.preventDefault();
            $diffField.find('li').removeClass('open');
          });
        }
      );
    }
  };

})(jQuery, Drupal, once);

