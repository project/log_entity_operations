# INTRODUCTION

Drupal by defaults adds a log message for entity operations when done from
interface. We miss these entries in logs when operations are performed through
code.

With data coming from remote systems or after complex operations performed in
your code, tracking changes on entities is impossible, as no log entries are
written. Most of the time, revisions are also disabled for custom entities to
prevent excessive storage usage.

This project aims to add log entries for all operations performed on entities 
of the selected entity bundles. Whenever you want to track operations that are
performed on an entity, you can enable logging of all changes that are being 
made. This helps you to get an overview on which changes are performed on your
entities.


## Example

If you're running an e-commerce site, you might synchronize your products
through an API. You do not want to enable revisions as you already have
revisions of your products in your external system - but you want to check
on which changes have been made on your products - and if the synchronization
still works as desired.

## Detailed Entity Logging

The submodule "Log Entity Operations to custom Entities" provides even more
comfort by providing a filterable overview over all logs made by this module.
This can help you tracking down changes when dealing, for example, with
missing data, where revisions are no longer available. You will find the
corresponding log entries on the given entity which also gives you the
information about which user deleted it and when.

 * Project page: https://drupal.org/project/log_entity_operations

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/log_entity_operations

# REQUIREMENTS
No special requirements.

# INSTALLATION

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

# CONFIGURATION

 * Configure the logging behavior through Administration > Configuration >
   Development > Log Entity Operations:

   * Specify wether the diff of the entity should be logged when the entity is
     being updated. This defaults to "Yes".

   * Select all entity types, respectively their bundles, that you want to keep
     track of.

# FAQ

Q: I enabled logging with diff but cannot see the diff on my log.

A: Ensure that the "Log diff of changes" setting is set to "Yes". Diffs are
   only logged on the update operation type. If the submodule "Log Entity
   Operations to custom Entities" is enabled, ensure that your user has the
   permission to view the log field ("View Entity Log Diff Field").

# MAINTAINERS

Current maintainers:
 * Nikunj Kotecha (nikunjkotecha) - https://www.drupal.org/user/694082
 * Florian Müller (fmueller_previon) - https://www.drupal.org/user/3524567
