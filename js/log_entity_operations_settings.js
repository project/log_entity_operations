(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.logEntityOperationsSettings = {
    attach: function (context, settings) {

      // Add "Select all" and "Clear all" links for all bundles of all entity
      // types (above all bundles).
      $(once('addSelectionLinks', '.all-selection-container')).each(function () {
        var $selectAll = $('<a></a>').text(
          Drupal.t('Select all types')
        ).on('click', function () {
          $('.log-entity-operations-settings-form .bundle-selection input[type="checkbox"]').prop('checked', true);
        });

        var $clearAll = $('<a></a>').text(
          Drupal.t('Clear all types')
        ).on('click', function () {
          $('.log-entity-operations-settings-form .bundle-selection input[type="checkbox"]').prop('checked', false);
        });

        $(this).append($selectAll).append($clearAll);
      });

      // Per entity type, add "Select all" and "Clear all" to select / deselect
      // all bundles of a given entity type.
      $(once('addSelectionLinks', '.bundle-selection fieldset')).each(function () {
        var $fieldset = $(this);

        var $selectAll = $('<a></a>').text(
          Drupal.t('Select all')
        ).on('click', function () {
          $fieldset.find('input[type="checkbox"]').prop('checked', true);
        });

        var $clearAll = $('<a></a>').text(
          Drupal.t('Clear all')
        ).on('click', function () {
          $fieldset.find('input[type="checkbox"]').prop('checked', false);
        });

        var $container = $('<div></div>').addClass('type-selection-handles');
        $fieldset.find('legend').after($container.append($selectAll).append($clearAll));
      });
    }
  };

})(jQuery, Drupal, once);
