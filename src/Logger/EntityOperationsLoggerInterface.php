<?php

namespace Drupal\log_entity_operations\Logger;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Interface EntityOperationsLoggerInterface.
 *
 * @package Drupal\log_entity_operations\Logger
 */
interface EntityOperationsLoggerInterface {

  /**
   * Log the performed operation on an entity.
   *
   * @param string $operation
   *   The operation which is performed on $entity_id.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The manipulated entity.
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user object that performed the $operation.
   * @param string $diff
   *   If configured, the diff is provided when $operation is update.
   */
  public function logUpdate(string $operation, EntityInterface $entity, AccountProxyInterface $user, string $diff = '');

}
