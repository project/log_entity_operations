<?php

namespace Drupal\log_entity_operations\Logger;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Class DrupalLogger.
 *
 * @package Drupal\log_entity_operations\Logger
 */
class DrupalLogger implements EntityOperationsLoggerInterface {
  use StringTranslationTrait;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * LogEntityOperationsManager constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(LoggerChannelInterface $logger,
                              TranslationInterface $string_translation) {
    $this->logger = $logger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Log update to Drupal::logger() channel.
   *
   * {@inheritDoc}
   */
  public function logUpdate(string $operation, EntityInterface $entity, AccountProxyInterface $user, string $diff = '') {
    $message = $this->t('@operation operation performed on entity of type: @type, bundle: @bundle, ID: @id for language @langcode performed by user @user.');
    $args = [
      '@operation' => $operation,
      '@type' => $entity->getEntityTypeId(),
      '@bundle' => $entity->bundle(),
      '@id' => $entity->id(),
      '@langcode' => $entity->language()->getId(),
      '@user' => $user->id() . (!empty($user->getDisplayName()) ? ' (' . $user->getDisplayName() . ')' : ''),
    ];

    if (!empty($diff)) {
      $message .= ' ' . $this->t('Diff: @diff');
      $args['@diff'] = $diff;
    }

    $this->logger->info($message, $args);
  }

}
