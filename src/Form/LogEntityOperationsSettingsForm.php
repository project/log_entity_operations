<?php

namespace Drupal\log_entity_operations\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LogEntityOperationsSettingsForm.
 */
class LogEntityOperationsSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * Name of the configuration YAML file.
   */
  const CONFIG_NAME = 'log_entity_operations.settings';

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Entity Type Bundle Info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  private $entityTypeBundleInfo;

  /**
   * Constructs a new LogEntityOperationsSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   Entity Type Bundle Info.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'log_entity_operations_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config(self::CONFIG_NAME);

    $form['#attached']['library'][] = 'log_entity_operations/settings_form';

    $form['intro_text'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>The options on this page allow you to precisely specify on which bundles you want operations to be logged.</p><p>If you need a simple overview over all logs for evidence purposes, consider enabling the <a href="/admin/modules">Log Entity Operations to custom Entities</a> submodule.</p>'),
      '#weight' => 1,
    ];

    $form['log_diff'] = [
      '#type' => 'select',
      '#title' => $this->t('Log diff of changes'),
      '#description' => $this->t('Choose if you want an entire diff of all changes in your logs.<br><em>Keep in mind that this might impact system performance when dealing with lots of large entities and huge change sets.</em>'),
      '#required' => TRUE,
      '#default_value' => (int) $config->get('log_diff'),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#weight' => 2,
    ];

    $form['log_diff_insert_delete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Also log diff when creating / deleting content'),
      '#description' => $this->t('If diff logging is enabled, you have the possibility to also log the entire content when it\'s being created or deleted. If you do not enable this selection, the diff will only be logged when the content changes.'),
      '#default_value' => (int) $config->get('log_diff_insert_delete'),
      '#weight' => 3,
      '#states' => [
        'visible' => [
          ':input[name="log_diff"]' => [
            'value' => 1
          ],
        ],
      ],
    ];

    $form['enable_collapsible_json'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Diff collapsible'),
      '#description' => $this->t('If enabled, the logged diff will be shown on the Entity Logs view collapsed to be easily analyzed. Otherwise it will be displayed as plain JSON.<br><strong>Only applies if the submodule "Log Entity Operations Entity" is installed and the log of changes above is enabled.</strong>'),
      '#default_value' => $config->get('enable_collapsible_json') ?: false,
      '#weight' => 4,
    ];

    $form['entity_bundles'] = [
      '#type' => 'details',
      '#title' => $this->t('Entity Bundles'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['bundle-selection'],
      ],
      '#weight' => 5,
    ];

    $form['entity_bundles']['bundles_description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p><em>Select below the entity bundles you want to be tracked.</em></p>') . '<div class="all-selection-container"></div>',
      '#weight' => 0,
    ];

    $bundles = [];

    foreach (array_keys($this->entityTypeManager->getDefinitions()) as $entity_type) {
      // Prevent Logging for the logs itself.
      if ($entity_type == 'entity_operation_log') {
        continue;
      }

      foreach (array_keys($this->entityTypeBundleInfo->getBundleInfo($entity_type)) as $bundle) {
        if (!array_key_exists($entity_type, $bundles)) {
          $bundles[$entity_type] = [];
        }
        $bundles[$entity_type][] = $bundle;
      }
    }

    $weight = 1;

    foreach ($bundles as $entity_type => $entity_bundles) {
      $options = $this->prepareOptions($entity_type, $entity_bundles);
      $default = $this->getEnabledFor($entity_type);
      $form['entity_bundles']['enabled_for_' . $entity_type] = [
        '#type' => 'checkboxes',
        '#title' => $entity_type,
        '#options' => $options,
        '#default_value' => $default,
        '#weight' => $weight,
      ];
      $weight++;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $enabled_for = [];

    foreach (array_keys($this->entityTypeManager->getDefinitions()) as $entity_type) {
      // Prevent Logging for the logs itself.
      if ($entity_type == 'entity_operation_log') {
        continue;
      }

      // Prevent the array prepared for array_merge from being null.
      $form_value = $form_state->getValue('enabled_for_' . $entity_type);
      if (empty($form_value)) {
        $form_value = [];
      }
      $values = array_values(array_filter($form_value));
      $enabled_for = array_merge($enabled_for, $values);
    }

    $config = $this->config(self::CONFIG_NAME);
    $config->set('log_diff', (bool) $form_state->getValue('log_diff'));
    $config->set('log_diff_insert_delete', (bool) $form_state->getValue('log_diff_insert_delete'));
    $config->set('enable_collapsible_json', (bool) $form_state->getValue('enable_collapsible_json'));
    $config->set('enabled_for', $enabled_for);
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get enabled bundles per entity type.
   *
   * @param string $entity_type
   *   The key of the entity type.
   *
   * @return array
   *   List of bundle names that are enabled.
   */
  private function getEnabledFor($entity_type) {
    $config = $this->config(self::CONFIG_NAME);
    $all = (array) $config->get('enabled_for');
    $type_bundles = [];
    foreach ($all as $type_bundle) {
      if (strpos($type_bundle, $entity_type . '.') !== FALSE) {
        $type_bundles[] = $type_bundle;
      }
    }
    return $type_bundles;
  }

  /**
   * Get Options for checkboxes form element.
   *
   * @param string $entity_type
   *   The Entity Type.
   * @param array $entity_bundles
   *   The Bundles of the given Entity.
   *
   * @return mixed
   *   A list of
   */
  private function prepareOptions($entity_type, array $entity_bundles) {
    $return_options = [];
    foreach ($entity_bundles as $value) {
      $return_options[$entity_type . '.' . $value] = $entity_type . '.' . $value;
    }
    return $return_options;
  }

}
