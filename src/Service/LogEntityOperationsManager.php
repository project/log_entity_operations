<?php

namespace Drupal\log_entity_operations\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\log_entity_operations\Logger\DrupalLogger;
use Drupal\log_entity_operations\ArrayDiff;
use Drupal\log_entity_operations\Form\LogEntityOperationsSettingsForm;

/**
 * Class LogEntityOperationsManager.
 *
 * @package Drupal\log_entity_operations\Service
 */
class LogEntityOperationsManager {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Drupal Logger.
   *
   * @var \Drupal\log_entity_operations\Logger\DrupalLogger
   */
  private $drupalLogger;

  /**
   * LogEntityOperationsManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\log_entity_operations\Logger\DrupalLogger $drupal_logger
   *   Config Factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              ConfigFactoryInterface $config_factory,
                              ModuleHandlerInterface $module_handler,
                              DrupalLogger $drupal_logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->drupalLogger = $drupal_logger;
  }

  /**
   * Log an update.
   *
   * This function checks wether logging should be done for this entity type
   * and prepares the information to be logged. Furthermore it decides which
   * logging routine will be used to store the logs.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity on which operations is performed.
   * @param string $operation
   *   Operation which is performed.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function dispatchLog(EntityInterface $entity, string $operation) {
    $type = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $allowed_for = $this->getConfig('enabled_for');

    // Prevent operations on our own log entities to trigger recursive logging.
    if ($type == 'entity_operation_log') {
      return;
    }

    // Confirm it is allowed for type and bundle.
    if (empty($allowed_for) || !in_array($type . '.' . $bundle, $allowed_for)) {
      return;
    }

    $user = \Drupal::currentUser();

    // Get the diff if available and allowed.
    $diff = '';
    if ($operation == 'update' && isset($entity->original) && $this->getConfig('log_diff')) {
      $array1 = $this->filterRecursive($entity->original->toArray());
      $updated_entity = $this->entityTypeManager->getStorage($type)->load($entity->id());
      $array2 = $this->filterRecursive($updated_entity->toArray());
      $differ = new ArrayDiff();
      $diff = json_encode($this->filterRecursive($differ->diff($array1, $array2)));
    }
    elseif (in_array($operation,['insert','delete']) && $this->getConfig('log_diff') && $this->getConfig('log_diff_insert_delete')) {
      $array = $this->filterRecursive($entity->toArray());
      $differ = new ArrayDiff();
      $diff = json_encode($this->filterRecursive($differ->diff([], $array)));
    }

    if ($this->moduleHandler->moduleExists('log_entity_operations_entity')) {
      // The submodule log_entity_operations_entity is enabled, so we use its
      // logger to store the information in custom entities.
      \Drupal::service('log_entity_operations_entity.logger')->logUpdate($operation, $entity, $user, $diff);
    }
    else {
      // We default to our Drupal logger channel.
      $this->drupalLogger->logUpdate($operation, $entity, $user, $diff);
    }
  }

  /**
   * Get value from config.
   *
   * @param string $key
   *   Key in config to get value for.
   *
   * @return array|mixed|null
   *   Value from config.
   */
  private function getConfig(string $key) {
    static $config;

    if (empty($config)) {
      $config = $this->configFactory->get(LogEntityOperationsSettingsForm::CONFIG_NAME);
    }

    return $config->get($key);
  }

  /**
   * Helper function to filter out empty values from array recursively.
   *
   * @param array $input
   *   Array to filter.
   *
   * @return array
   *   Filtered array.
   */
  private function filterRecursive(array $input): array {
    foreach ($input as &$value) {
      if (is_array($value)) {
        $value = $this->filterRecursive($value);
      }
    }

    return array_filter($input);
  }

}
